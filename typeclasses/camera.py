"""
Evennia Camera Object

Contribution - Cloud_Keeper 2017

Take an in game photograph.

A camera object is a custom typeclass with a "use_object" method and a
"photograph" command. The command takes a list of subjects (by default
the caller.location.contents) and provides that list to camera.use_object().

The camera.use_object() method creates a photograph object using a custom
typeclass and provides the object with the caller.location description and
a dictionary of subject.key: subject.description. This information is recorded
at the time of execution and will not be later updated by the game.

The photograph object has a modified return_appearance method which returns
an EvMenu. The menu initially presents the looker with the recorded location
description and allows looking at the individual subjects, recreating what
interacting with that location with the look command would have been like
at the time the photograph was taken.

Usage - In-game:
    @create camera:Evennia.contrib.camera.Camera
    selfie

"""
import time
from django.conf import settings
from evennia.utils import utils, evmenu
from evennia import CmdSet
from evennia.utils.create import create_object
from typeclasses.objects import Object

COMMAND_DEFAULT_CLASS = utils.class_from_module(settings.COMMAND_DEFAULT_CLASS)

# Used by CmdUse
_TGT_ERRMSG = "'{}' could not be located."


class CameraCmdSet(CmdSet):
    """
    Camera Command Set
    """
    key = "cameracmdset"
    priority = 1

    def at_cmdset_creation(self):
        self.add(CmdPhotograph())


class CmdPhotograph(COMMAND_DEFAULT_CLASS):
    """
    Take a photograph.

    Usage:
        Photograph <subject>, <subject>, <subject>....]


    """
    key = "photograph"
    aliases = ["snap", "selfie"]
    locks = "cmd:all()"
    help_category = "General"

    def func(self):
        """Pass specified subjects to obj or default to location.contents"""

        subjectlist = []
        for subject in self.lhslist:
            if subject:
                subject = self.caller.search(subject, nofound_string=_TGT_ERRMSG.format(subject))
            if subject:
                subjectlist.append(subject)
        if not subjectlist:
            subjectlist = self.caller.location.contents

        self.obj.use_object(self.caller, subjectlist)


class Camera(Object):

    def at_object_creation(self):
        self.cmdset.add_default(CameraCmdSet, permanent=True)
        super(Camera, self).at_object_creation()

    def use_object(self, character, subjects):

        # Initialise photograph object.
        photo = create_object(typeclass=Photograph, location=character,
                              key="Photo " + character.location.key + str(int(time.time())))
        photo.db.desc = "A small polaroid picture."
        photo.db.image = ("Captured in the glossy polaroid is: \n" +
                          character.location.key + "\n" +
                          character.location.db.desc)
        photo.db.subjects = {subject.key: subject.db.desc for subject in list(set(subjects))}
        character.location.msg_contents(character.key + " snapped a photograph!")


def photograph_node_formatter(nodetext, optionstext, caller=None):
    """
    A minimalistic node formatter, no lines or frames.
    """
    return nodetext + "\n" + optionstext


def photograph_options_formattter(optionlist, caller=None):
    """
    A minimalistic options formatter that mirrors a rooms content list.
    """
    if not optionlist:
        return ""

    return "You see: " + ", ".join([key for key, msg in optionlist])


class Photograph(Object):

    def return_appearance(self, looker):

        # Initialise photograph menu.
        evmenu.EvMenu(looker, "typeclasses.camera",
                      startnode="photograph_node", persistent=True,
                      cmdset_mergetype="Replace",
                      node_formatter=photograph_node_formatter,
                      options_formatter=photograph_options_formattter,
                      photodesc=self.db.image, subjects=self.db.subjects,
                      current_state="")
        return ""


def photograph_node(caller, input_string):
    menu = caller.ndb._menutree
    options = [{"key": "_default",
                "goto": "photograph_node"}]

    if not input_string or input_string not in menu.subjects:
        if menu.current_state != "Main":
            # Display main photo description
            text = menu.photodesc
            menu.current_state = "Main"
            for subject in menu.subjects:
                _dict = {}
                _dict["key"] = subject
                _dict["goto"] = "photograph_node"
                options.append(_dict)
        else:
            # Display error message
            text = "Choose an option or 'Quit' to stop viewing."

    else:
        # An item was selected to focus, display item description.
        text = ("Looking closely at '" + input_string + "' you see: \n" +
                input_string.title() + "\n" + menu.subjects[input_string])
        menu.current_state = input_string

    return text, options
