This is a camera system. It consists of a Camera object which has a "use" command (In my projects I have a general use command which basically calls objects at_use() method). The object creates a photograph object. Looking at a photograph opens up an EvMenu which fakes how a given location looked (including it's contents and their descriptions) at the time the photo was taken. 

Files of interest:

typeclasses/camera - contains all the work.


Example use:

/>Look photograph

-------------------------------------------------------------------
Limbo(#2)

This is a location description at the time the photo was taken.

Bla bla bla bla bla bla bla bla bla bla bla bla.

-------------------------------------------------------------------

You see: Object1, Object2, Character1

/>Character1

-------------------------------------------------------------------
Character(#2)

This is the characters description at the time the photo was taken.

Bla bla bla bla bla bla bla bla bla bla bla bla.

-------------------------------------------------------------------